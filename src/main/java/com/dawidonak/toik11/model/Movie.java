package com.dawidonak.toik11.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Movie {
    private int movieId;
    private String title;
    private int year;
    private String image;


}

