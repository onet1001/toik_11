package com.dawidonak.toik11.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MovieDto {
    private int movieId;
    private String title;
    private int year;
    private String image;


}
