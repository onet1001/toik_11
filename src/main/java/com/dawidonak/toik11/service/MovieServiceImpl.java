package com.dawidonak.toik11.service;

import com.dawidonak.toik11.dto.MovieDto;
import com.dawidonak.toik11.model.Movie;
import com.dawidonak.toik11.repository.MovieRepository;
import com.dawidonak.toik11.repository.MovieRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MovieServiceImpl implements MovieService{

    @Autowired
    private MovieRepository movieRepository;

    public Map<String, List<MovieDto>> getMovieList()
    {
        Map<String, List<MovieDto>> map= new HashMap<>();
        ArrayList<MovieDto> movieList= new ArrayList<>();
        for (Movie movie: movieRepository.getMovieList()) {
            MovieDto movieDto= new MovieDto();
            movieDto.setMovieId(movie.getMovieId());
            movieDto.setTitle(movie.getTitle());
            movieDto.setYear(movie.getYear());
            movieDto.setImage(movie.getImage());
            movieList.add(movieDto);
        }
        map.put("movies",movieList);
        return map;
    }
    
}
