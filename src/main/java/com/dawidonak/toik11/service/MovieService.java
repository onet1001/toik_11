package com.dawidonak.toik11.service;

import com.dawidonak.toik11.dto.MovieDto;
import com.dawidonak.toik11.model.Movie;

import java.util.List;
import java.util.Map;

public interface MovieService {
    Map<String, List<MovieDto>> getMovieList();
}
