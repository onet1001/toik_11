package com.dawidonak.toik11.rest;

import com.dawidonak.toik11.dto.MovieDto;
import com.dawidonak.toik11.service.MovieService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class MovieRestController {

    @Autowired
    private MovieService movieService;

    @CrossOrigin
    @GetMapping("/movies")
    public ResponseEntity<Map<String, List<MovieDto>>> returnMovies(){
        return new ResponseEntity<>(movieService.getMovieList(), HttpStatus.OK);
    }

}
