package com.dawidonak.toik11.repository;

import com.dawidonak.toik11.model.Movie;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class MovieRepositoryImpl implements MovieRepository{

    private List<Movie> movieList;


    public MovieRepositoryImpl() {
        movieList= new ArrayList<>();
        Movie movie = new Movie();
        Movie movie1 =new Movie();
        Movie movie2 =new Movie();
        Movie movie3 =new Movie();
        movie.setMovieId(1);
        movie.setTitle("Piraci z krzemowej doliny");
        movie.setYear(1999);
        movie.setImage("https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg");
        movieList.add(movie);
        //Movie 2
        movie1.setMovieId(2);
        movie1.setTitle("Ja, robot");
        movie1.setYear(2004);
        movie1.setImage("https://fwcdn.pl/fpo/54/92/95492/7521206.6.jpg");
        movieList.add(movie1);
        //Movie3
        movie2.setMovieId(3);
        movie2.setTitle("Kod nieśmiertelności");
        movie2.setYear(2011);
        movie2.setImage("https://fwcdn.pl/fpo/89/67/418967/7370853.6.jpg");
        movieList.add(movie2);
        //Movie4
        movie3.setMovieId(4);
        movie3.setTitle("Ex Machina");
        movie3.setYear(2015);
        movie3.setImage("https://fwcdn.pl/fpo/64/19/686419/7688121.6.jpg");
        movieList.add(movie3);

    }

    public List<Movie> getMovieList() {
        return movieList;
    }
}
