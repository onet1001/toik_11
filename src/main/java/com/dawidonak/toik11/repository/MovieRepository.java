package com.dawidonak.toik11.repository;

import com.dawidonak.toik11.model.Movie;

import java.util.List;

public interface MovieRepository {
    List<Movie> getMovieList();
}
